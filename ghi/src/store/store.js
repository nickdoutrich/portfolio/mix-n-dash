import { configureStore } from "@reduxjs/toolkit";
import { setupListeners } from "@reduxjs/toolkit/query";
import { accountsApi } from "./accountsApi";
import { bartendersApi } from "./bartendersApi";
import { eventsApi } from "./eventsApi";

export const store = configureStore({
  reducer: {
    [accountsApi.reducerPath]: accountsApi.reducer,
    [bartendersApi.reducerPath]: bartendersApi.reducer,
    [eventsApi.reducerPath]: eventsApi.reducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware()
      .concat(accountsApi.middleware)
      .concat(bartendersApi.middleware)
      .concat(eventsApi.middleware),
  devTools: process.env.NODE_ENV !== "production",
});

setupListeners(store.dispatch);
