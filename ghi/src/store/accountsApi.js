import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const accountsApi = createApi({
  reducerPath: "accounts",
  baseQuery: fetchBaseQuery({
    baseUrl: process.env.REACT_APP_MIX_API_HOST,
    prepareHeaders: (headers, { getState }) => {
      const selector = accountsApi.endpoints.getToken.select();
      const { data } = selector(getState());
      if (data && data.access_token) {
        headers.set("Authorization", `Bearer ${data.access_token}`);
      }
      return headers;
    },
  }),
  tagTypes: ["Token", "User", "UserDetail"],
  endpoints: (builder) => ({
    logout: builder.mutation({
      query: () => {
        return {
          url: "/token",
          method: "delete",
          credentials: "include",
        };
      },
      invalidatesTags: (result) => {
        return (result && ["User", "Token", "UserDetail"]) || [];
      },
    }),
    login: builder.mutation({
      query: (info) => {
        let formData = null;
        if (info instanceof HTMLElement) {
          formData = new FormData(info);
        } else {
          formData = new FormData();
          formData.append("username", info.email);
          formData.append("password", info.password);
        }
        return {
          url: "/token",
          method: "post",
          body: formData,
          credentials: "include",
        };
      },
      providesTags: ["User"],
      invalidatesTags: (result) => {
        return (result && ["Token", "UserDetail"]) || [];
      },
    }),
    createUser: builder.mutation({
      query: (data) => ({
        url: "/api/users",
        body: {
          first: data.first,
          last: data.last,
          phone_number: data.phoneNumber,
          email: data.email,
          password: data.password,
          state: data.state,
          city: data.city.toLowerCase(),
        },
        method: "post",
      }),
      invalidatesTags: ["UserList"],
    }),
    getUser: builder.query({
      query: () => ({
        url: "/api/users/detail",
        credentials: "include",
      }),
      providesTags: ["UserDetail"],
    }),
    updateUser: builder.mutation({
      query: (data) => ({
        url: "/api/users/detail",
        body: {
          first: data.first,
          last: data.last,
          phone_number: data.phoneNumber,
          email: data.email,
          password: data.password,
          state: data.state,
          city: data.city.toLowerCase(),
        },
        credentials: "include",
        method: "put",
      }),
      invalidatesTags: (result) => {
        return (result && ["UserDetail"]) || [];
      },
    }),
    getUsers: builder.query({
      query: () => "/api/users",
      providesTags: ["UserList"],
    }),
    getToken: builder.query({
      query: () => {
        return {
          url: "/token",
          credentials: "include",
        };
      },
      providesTags: ["Token"],
    }),
  }),
});

export const {
  useLoginMutation,
  useGetTokenQuery,
  useLogoutMutation,
  useGetUserQuery,
  useUpdateUserMutation,
  useCreateUserMutation,
  useGetUsersQuery,
} = accountsApi;
