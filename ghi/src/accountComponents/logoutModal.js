import "../css/index.css";
import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { useLogoutMutation } from "../store/accountsApi";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";

function LogoutModal() {
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const navigate = useNavigate();
  const [logout, result] = useLogoutMutation();
  const [error, setError] = useState("");

  async function handleSubmit(e) {
    e.preventDefault();
    handleClose();
    logout();
  }

  useEffect(() => {
    if (result.isSuccess) {
      navigate("/");
    } else if (result.isError) {
      setError(result.error);
      console.log(error);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [result]);

  return (
    <>
      <Button
        style={{ color: "#F4EBD0" }}
        className="dropdown-item nav-item"
        variant="primary"
        onClick={handleShow}
      >
        Logout
      </Button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Logout</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="container">
            <div className="columns is-centered">
              <div className="column is-one-third">
                <button className="btn btn-primary" onClick={handleSubmit}>
                  Logout
                </button>
              </div>
            </div>
          </div>
        </Modal.Body>
        <Modal.Footer></Modal.Footer>
      </Modal>
    </>
  );
}

export default LogoutModal;
