import "../css/index.css";
import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { useDeleteEventMutation } from "../store/eventsApi";
import Modal from "react-bootstrap/Modal";

function DeleteEventModal(eventId) {
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const navigate = useNavigate();
  const [deleteEvent, result] = useDeleteEventMutation();
  const [error, setError] = useState("");

  async function handleSubmit(e) {
    e.preventDefault();
    handleClose();
    deleteEvent(eventId.eventId);
  }

  useEffect(() => {
    if (result.isSuccess) {
      navigate("/account/detail");
    } else if (result.isError) {
      setError(result.error);
      console.log(error);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [result]);

  return (
    <>
      <button
        type="button"
        style={{
          padding: ".25rem .4rem",
          fontSize: ".875rem",
          lineHeight: "1.25",
          borderRadius: ".2rem",
        }}
        className="btn btn-danger"
        onClick={handleShow}
      >
        Cancel Event
      </button>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Are you sure?</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="container">
            <div className="columns is-centered">
              <div className="column is-one-third">
                <button className="btn btn-danger" onClick={handleSubmit}>
                  Cancel Event
                </button>
              </div>
            </div>
          </div>
        </Modal.Body>
        <Modal.Footer></Modal.Footer>
      </Modal>
    </>
  );
}

export default DeleteEventModal;
