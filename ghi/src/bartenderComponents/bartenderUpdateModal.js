import "../css/index.css";
import React, { useState, useEffect } from "react";
import AsyncSelect from "react-select/async";
import makeAnimated from "react-select/animated";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import { useNavigate } from "react-router-dom";
import { useGetUserQuery } from "../store/accountsApi";
import {
  useGetBartendersQuery,
  useUpdateBartenderMutation,
} from "../store/bartendersApi";

function UpdateBartenderModal() {
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const { data, isLoading } = useGetUserQuery();

  const { data: bartenderData } = useGetBartendersQuery();
  const userBartender = bartenderData?.bartenders?.find(function (bartender) {
    return bartender.user.id === data?.id;
  });
  const [error, setError] = useState("");
  const navigate = useNavigate();
  const [update, result] = useUpdateBartenderMutation();
  const [about, setAbout] = useState("");
  const [drinks, setDrinks] = useState("");
  const animatedComponent = makeAnimated();
  const loadOptions = async (inputText, callback) => {
    const response = await fetch(
      `https://www.thecocktaildb.com/api/json/v1/1/search.php?s=${inputText}`
    );
    const json = await response.json();
    callback(
      json.drinks.map((i) => ({
        value: i.strDrink,
        label: i.strDrink,
        image: i.strDrinkThumb,
      }))
    );
  };

  async function handleSubmit(e) {
    e.preventDefault();
    let drinksList = [];
    for (let drink of drinks) {
      drinksList.push(drink.label);
    }
    update({ about, drinksList });
    setAbout(about);
  }
  useEffect(() => {
    if (result.isSuccess) {
      handleClose();
      navigate("/account/detail");
    } else if (result.isError) {
      setError(result.error);
      console.log(error);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [result]);

  if (isLoading) {
    return <progress className="progress is-primary" max="100"></progress>;
  }

  return (
    <>
      <Button
        variant="primary"
        onClick={() => {
          setAbout(userBartender.about);
          handleShow();
        }}
        style={{ marginLeft: "10px", fontSize: "14px" }}
      >
        Update Bartender Info
      </Button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Update Bartender</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form onSubmit={handleSubmit}>
            <textarea
              rows={3}
              required={true}
              value={about}
              onChange={(e) => setAbout(e.target.value)}
              id={about}
              placeholder="This is what your customers will see."
            />
            <label htmlFor={drinks}>{"Drinks"}</label>
            <AsyncSelect
              theme={(theme) => ({
                ...theme,
                borderRadius: 0,
                colors: {
                  ...theme.colors,
                  primary25: "#B88A44",
                  primary: "#122620",
                },
              })}
              isMulti
              components={animatedComponent}
              className="mb-3"
              loadOptions={loadOptions}
              placeholder={"search for your drinks and click to add..."}
              value={drinks}
              onChange={setDrinks}
            />
            <div className="field">
              <button className="btn btn-primary">Update</button>
            </div>
          </form>
        </Modal.Body>
        <Modal.Footer></Modal.Footer>
      </Modal>
    </>
  );
}

export default UpdateBartenderModal;
