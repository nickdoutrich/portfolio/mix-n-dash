import AsyncSelect from "react-select/async";
import makeAnimated from "react-select/animated";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { useCreateBartenderMutation } from "../store/bartendersApi";

function BartenderSignupModal() {
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const [error, setError] = useState("");
  const navigate = useNavigate();
  const [signup, result] = useCreateBartenderMutation();
  const [about, setAbout] = useState("");
  const [drinks, setDrinks] = useState([]);
  const animatedComponent = makeAnimated();
  const loadOptions = async (inputText, callback) => {
    const response = await fetch(
      `https://www.thecocktaildb.com/api/json/v1/1/search.php?s=${inputText}`
    );
    const json = await response.json();
    callback(
      json.drinks.map((i) => ({
        value: i.strDrink,
        label: i.strDrink,
        image: i.strDrinkThumb,
      }))
    );
  };
  async function handleSubmit(e) {
    e.preventDefault();
    let drinksList = [];
    for (let drink of drinks) {
      drinksList.push(drink.label);
    }
    signup({ about, drinksList });
  }
  useEffect(() => {
    if (result.isSuccess) {
      handleClose();
      setAbout("");
      setDrinks([]);
      navigate("/account/detail");
    } else if (result.isError) {
      setError(result.error);
      console.log(error);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [result]);

  return (
    <>
      <Button
        variant="primary"
        onClick={handleShow}
        style={{ marginLeft: "10px", fontSize: "14px" }}
      >
        Become a bartender
      </Button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>New bartender</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form onSubmit={handleSubmit}>
            <label htmlFor={about}>{"About"}</label>
            <textarea
              rows={3}
              required={true}
              value={about}
              onChange={(e) => setAbout(e.target.value)}
              id={about}
              placeholder="This is what your customers will see."
            />
            <label htmlFor={drinks}>{"Drinks"}</label>
            <AsyncSelect
              theme={(theme) => ({
                ...theme,
                borderRadius: 0,
                colors: {
                  ...theme.colors,
                  primary25: "#B88A44",
                  primary: "#122620",
                },
              })}
              isMulti
              components={animatedComponent}
              className="mb-3"
              loadOptions={loadOptions}
              placeholder={"search for your drinks and click to add..."}
              value={drinks}
              onChange={setDrinks}
            />
            <div className="field">
              <button type="submit" className="btn btn-primary">
                Register bartender
              </button>
            </div>
          </form>
        </Modal.Body>
        <Modal.Footer></Modal.Footer>
      </Modal>
    </>
  );
}

export default BartenderSignupModal;
