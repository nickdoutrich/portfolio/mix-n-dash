## Week One

### November / 14 / 2022

As a Developer, I want to find a bootstrap for the front-end to make website user friendly while also
providing a lot of features. My team and I have already constructed a blueprint for the website and since we
are a head of schedule we decided to get on the front-end
Little nervous but the project is doable, so far I ran into some issues with docker and it seems that I am
the only one with this issue. I talked to software engineers outside of my team and to my luck there so
clear solution. However, this did not stop me to contribute to my team. I have provided some resources to
my team to help design the platform along with help modify some of the components.
main page inspiration: https://startbootstrap.com/theme/landing-page

### November / 15 / 2022

took the time to study the concept of FastAPI and MongoDB, however, after discussing about the database we decided to have the data base be PostgreSQL since its fit for our platform. We were able to receive more lecture from Hack Reactor to get a better idea on how to setup our platform

### November / 16 / 2022

this morning's practice in Hack Rector made it easy to understand the code.
Had our first stand up and our goal for today to have a working endpoint and setting up our roles in the
projects. So far we have came to a conclusion that its best to have two teams, the back-end and front-end. At
the end of the day we will present what we have done and then will demonstrate how we did it so that everyone
learns something. My role in the team is to have the front end functional and pleasant to see, little nervous
because I need to learn more about react hooks but I'm sure everything will work out in the end.
Some of the challenges I faced throughout the day is figuring out were to start. I found my self to be lost a
lot of the times, however, thanks to my team I am able to get a sense of how everything works. Now that its
near the end of the day I plan to start catching up on FastAPI

update: I was able to finish off most of hosts backend to semi-work, fortunately Nick was available so I sent
him the code in a direct message to double check my progress.

### November / 17 / 2022

Nick worked more on the project last night and got my code to work along with user authentication. Since
everyone is in a different point of what is happening we all agreed to take the time to learn what we have
done so far. What I'm going to prioritize is reviewing the api and how each line works, later today Jose and I
are going to study React Hooks since it may play a big role in this project. I went ahead and found some components that we may use in order to ensure that everything works fine.

### November / 18 / 2022

Jose and I spent the day researching and planning out the design of the entire website, so far its going ok but a lot of templates we were seeing will not be compatible since they're using another language that's
going to be hard to translate. In the weekend me and Jose plan to meet up to finish the forms and nav bar.
The nav bar was the most challenging part so far since the website keeps crashing and I'm trying to figure
out how I even implemented it in the last project.

## Week Two

### November / 21 / 2022

on the Weekend Jose and I came with no result and we feel really bad because the backend is almost done while
the front-end needs a lot of work. Fortunately after thoroughly researching what went wrong I figured out why
the website kept crashing, that the other components were not complete. After I fixed it the nav bar finally
came to life, the sad part is that there is not much to it since we need to discuss more of the nav bar since
we haven't talk in-depth how the nav bar should work. We didn't just stop there because Jose and I finished
the forms, there are static but it looks nice.

### November / 22 / 2022

As time went one I feel like I'm behind, jose and I started working on the front-end more heavily and we
installed tailwind and a calender module. We talked about adding a calender for users to book bartenders,
however, by the end of the day we decided to scratch it off so the day came from being productive to
unproductive. ;-;

### November / 23 / 2022

We decided to spend the day to catch up on learning so me and Jose decided to watch some Udemy courses and
youtube videos about bootstrap, mui, and tailwind. From there we started working on improving some of the CSS
with the forms. Jose and I finally finalize the calender and we're both happy about it

## Week Three

### November / 28 / 2022

Turns out the calender is not really necessary so we had to scrap the idea. Was it sad? not really since I
have a better understanding of how to use TailWind. Since we are going to do React Hooks for the whole
projects I realized that we need to change all components to function-based. During the day I also took the
time to create selected checkboxes in array in testBartender updated the app.js.

### November / 29 / 2022

My instructor Riley gave a lecture about testing the backend and fortunately I managed to keep up so we now
have a test. Unfortunately, the test does not work, fortunately Nick was able to get his working and we
agreed to save the testing for the last week. I also went ahead and did major changes in the main page and
slightly modified the nav bar. We changed the color palette from blue and white to now gold and black which
looks really neat since we want our website to be nice.

### December / 01 / 2022

Decided to take the time to upgrade the navigation bar. The plan is to make sure that if the user is logged
in then they will be able to see other options such as account information and logout. users who are not
logged in they will see the login, sign up, and bartender list. However, for some reason I am not able to log
in and I was confused because the feature is working for Nick and Jose. So I contacted my instructor Dalonte
to see if there is something wrong with the code. We essential spent the whole day trying to figure out what
was causing the CORS issues, the code seems to be correct. We decided to find the answer tomorrow, in the
meantime I was able to updated part of the CSS when a user is not logged in. I also redesigned the page so if
the person tries to go to a

### December / 02 / 2022

In the morning Riley was able to help me out. Turns out it was the Docker image because I forgot that a
Docker image is a read-only template that contains the instructions for building and running a Docker
container. It is a package that contains everything a container needs to run, including the code, libraries,
dependencies, and runtime environment. so my docker image was outdated, once I created the image using the
command 'docker-compose build' all the CORS issues went away. I couldn't thank enough my instructors for
taking the time to assist me.

### December / 03 / 2022

Jose and I noticed that in the original wire frame there are some functionality that we agreed to not
implement due to the time constraint. We took the time to figure out what the design is going to look like.

### December / 05 / 2022

Made huge upgrades to the detail and bartender page and updated the error pop up. Nick and I worked on the
same files so we had to make sure that there are no conflicts that will break the website. Unfortunately, I
am the downfall and merged the incorrect information so we had to spend an hour trying to fix this.
Afterwards everything went well and fixed more of the design.

### December / 06 / 2022

The team and I decided to take a look to see what me and Jose could improve on. Ray and Nick gave some
feedback so the day was spent redesigning and researching to make their idea a reality.

### December / 07 / 2022

There was an issue where he background image did not work properly so I had to figure out a way to fix it.
After some research, there isn't really a solution for this since in react the background image would only be
partially visible. To compact this solution I had to changed the background image height to auto and that
seemed to work for now. Afterwards Jose and I got the bartender account information and CSS to work which
we're really proud about. Then I went ahead and cleaned up some of the unused code.

### December / 08 / 2022

Nick was able to help me out with the bartender account so if a bartender wants to see their profile from a
user perspective they will have a button that will state "view profile" below the information. From there
since it is the last full week we have gotten started with the testing. I managed to get mine to almost work
and luckily ray stepped in and fixed the issue for me. Turns out, the test was trying to see if there is a
custom user the data.
