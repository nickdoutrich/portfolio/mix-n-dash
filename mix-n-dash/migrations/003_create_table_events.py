steps = [
    [
        """
        CREATE TABLE events (
            id SERIAL NOT NULL UNIQUE,
            address TEXT NOT NULL,
            city TEXT NOT NULL,
            state TEXT NOT NULL,
            date DATE NOT NULL,
            time TIME NOT NULL,
            user_id INTEGER NOT NULL REFERENCES users("id") ON DELETE CASCADE,
            bartender_id INTEGER NOT NULL REFERENCES bartenders("id") ON DELETE CASCADE
        );
        """,
        """
        DROP TABLE events;
        """
    ]
]
