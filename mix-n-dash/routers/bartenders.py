from fastapi import APIRouter, Depends, Response, status, HTTPException
from .auth import authenticator
from queries.bartenders import BartenderQueries
from models import (
    BartenderIn,
    BartenderOut,
    BartendersOut,
    BartenderUpdate,
    DrinksOut,
)


router = APIRouter()


@router.get("/api/bartenders/", response_model=BartendersOut)
def bartenders_list(queries: BartenderQueries = Depends()):
    return {
        "bartenders": queries.get_all_bartenders(),
    }


@router.get("/api/drinks/", response_model=DrinksOut)
def drinks_list(queries: BartenderQueries = Depends()):
    return {
        "drinks": queries.get_all_drinks(),
    }


@router.post("/api/bartenders/", response_model=BartenderOut)
async def create_bartender(
    response: Response,
    bartender_in: BartenderIn,
    queries: BartenderQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return queries.create_bartender(bartender_in, account_data.get("id"))


@router.get("/api/bartenders/{bartender_id}", response_model=BartenderOut)
def get_bartender(
    bartender_id: int,
    response: Response,
    queries: BartenderQueries = Depends(),
):
    record = queries.get_bartender(bartender_id)
    if record is None:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Bartender does not exist",
        )
    else:
        return record


@router.put("/api/bartender/", response_model=BartenderOut)
async def update_bartender(
    bartender_in: BartenderUpdate,
    response: Response,
    queries: BartenderQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return queries.update_bartender(account_data.get("id"), bartender_in)


@router.delete("/api/bartender/", response_model=bool)
async def delete_bartender(
    queries: BartenderQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    queries.delete_bartender(account_data.get("id"))
    return True
