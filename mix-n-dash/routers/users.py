from fastapi import (
    Depends,
    HTTPException,
    status,
    Response,
    APIRouter,
    Request,
)
from jwtdown_fastapi.authentication import Token
from .auth import authenticator
from queries.users import UserQueries, DuplicateUserError
from models import UserIn, UserOut, UsersOut
from pydantic import BaseModel

router = APIRouter()


class UserForm(BaseModel):
    username: str
    password: str


class UserToken(Token):
    user: UserOut


class HttpError(BaseModel):
    detail: str


@router.get("/api/users/", response_model=UsersOut)
def users_list(queries: UserQueries = Depends()):
    return {
        "users": queries.get_all_users(),
    }


@router.post("/api/users/", response_model=UserToken | HttpError)
async def create_user(
    info: UserIn,
    request: Request,
    response: Response,
    queries: UserQueries = Depends(),
):
    hashed_password = authenticator.hash_password(info.password)
    try:
        user = queries.create_user(info, hashed_password)
    except DuplicateUserError:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Cannot create an account with those credentials",
        )
    form = UserForm(username=info.email, password=info.password)
    token = await authenticator.login(response, request, form, queries)
    return UserToken(user=user, **token.dict())


@router.get("/token", response_model=UserToken | None)
async def get_token(
    request: Request,
    user: dict = Depends(authenticator.try_get_current_account_data),
) -> UserToken | None:
    if user and authenticator.cookie_name in request.cookies:
        return {
            "access_token": request.cookies[authenticator.cookie_name],
            "type": "Bearer",
            "user": user,
        }


@router.get("/api/users/detail", response_model=UserOut | HttpError)
async def get_user(
    # user_id: int,
    # replaced user_id with id from accountdata get
    response: Response,
    queries: UserQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    record = queries.get(account_data.get("id"))
    if record is None:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="User does not exist",
        )
    else:
        # if user_id == account_data.get("id"):
        return record
        # else:
        #     raise HTTPException(
        #         status_code=status.HTTP_400_BAD_REQUEST,
        #         detail="Invalid credentials",
        #     )


@router.put("/api/users/detail", response_model=UserOut | HttpError)
# {user_id}
async def update_user(
    # user_id: int,
    # replaced user_id with id from accountdata get
    user_in: UserIn,
    response: Response,
    queries: UserQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    hashed_password = authenticator.hash_password(user_in.password)
    # if user_id == account_data.get("id"):
    record = queries.update_user(
        account_data.get("id"), user_in, hashed_password
    )
    if record is None:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="User does not exist",
        )
    else:
        return record
    # else:
    #     raise HTTPException(
    #         status_code=status.HTTP_400_BAD_REQUEST,
    #         detail="Invalid credentials",
    #     )


@router.delete("/api/users/detail", response_model=bool)
async def delete_user(
    # user_id: int,
    queries: UserQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    # if user_id == account_data.get("id"):
    queries.delete_user(account_data.get("id"))
    # else:
    #     raise HTTPException(
    #         status_code=status.HTTP_400_BAD_REQUEST,
    #         detail="Invalid credentials",
    #     )
    return True
