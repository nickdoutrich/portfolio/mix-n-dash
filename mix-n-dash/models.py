from pydantic import BaseModel
from datetime import date, time


class UserIn(BaseModel):
    first: str
    last: str
    phone_number: str
    email: str
    password: str
    state: str
    city: str


class UserBartenderEventOut(BaseModel):
    first: str
    last: str
    phone_number: str
    email: str


class UserOut(BaseModel):
    id: int
    first: str
    last: str
    phone_number: str
    email: str
    state: str
    city: str


class UsersOut(BaseModel):
    users: list[UserOut]


class DrinkOut(BaseModel):
    id: int
    name: str
    image: str


class DrinksOut(BaseModel):
    drinks: list[DrinkOut]


class BartenderIn(BaseModel):
    about: str
    drinks: list
    # user_id: int


class BartenderOut(BaseModel):
    id: int
    about: str
    drinks: list
    user: UserOut


class BartenderEventOut(BaseModel):
    id: int
    drinks: list
    bartender_info: UserBartenderEventOut


class BartenderUpdate(BaseModel):
    about: str
    drinks: list


class BartendersOut(BaseModel):
    bartenders: list[BartenderOut]


class EventIn(BaseModel):
    address: str
    city: str
    state: str
    date: date
    time: time
    # user_id: int
    bartender_id: int


class EventOut(BaseModel):
    id: int
    address: str
    city: str
    state: str
    date: date
    time: time
    bartender: BartenderEventOut
    user: UserOut


class EventUpdate(BaseModel):
    address: str
    city: str
    state: str
    date: date
    time: time


class EventsOut(BaseModel):
    events: list[EventOut]
