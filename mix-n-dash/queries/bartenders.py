import os
from psycopg_pool import ConnectionPool

pool = ConnectionPool(conninfo=os.environ["DATABASE_URL"])


class BartenderQueries:
    def get_all_bartenders(self):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT u.id AS user_id, u.first, u.last,
                    u.phone_number, u.email, u.city, u.state,
                        b.id AS bartender_id, b.about, b.drinks
                    FROM users u
                    INNER JOIN bartenders b ON u.id = b.user_id

                    GROUP BY u.id, u.first, u.last, u.phone_number,
                    u.email, u.city, u.state,
                        b.id, b.about, b.drinks

                    ORDER BY b.id
                """
                )

                results = []
                rows = cur.fetchall()
                for row in rows:
                    bartender = self.bartender_record_to_dict(
                        row, cur.description
                    )
                    results.append(bartender)
                return results

    def get_bartender(self, bartender_id):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT u.id AS user_id, u.first, u.last, u.phone_number,
                    u.email, u.password, u.city, u.state,
                        b.id AS bartender_id, b.about, b.drinks
                    FROM users u
                    INNER JOIN bartenders b ON u.id = b.user_id
                    WHERE b.id = %s
                """,
                    [bartender_id],
                )
                row = cur.fetchone()
                return self.bartender_record_to_dict(row, cur.description)

    def create_bartender(self, data, user_id):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                print(data.drinks)
                params = [
                    data.about,
                    data.drinks,
                    user_id,
                ]
                cur.execute(
                    """
                    INSERT INTO bartenders (about, drinks, user_id)
                    VALUES (%s, %s, %s)
                    RETURNING id, about, drinks, user_id
                    """,
                    params,
                )

                row = cur.fetchone()
                id = row[0]

        if id is not None:
            return self.get_bartender(id)

    def update_bartender(self, user_id, data):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                params = [data.about, data.drinks, user_id]
                cur.execute(
                    """
                    UPDATE bartenders
                    SET about = %s, drinks = %s
                    WHERE user_id = %s
                    RETURNING id, about, drinks
                    """,
                    params,
                )

                row = cur.fetchone()
                id = row[0]
        if id is not None:
            return self.get_bartender(id)

    def delete_bartender(self, user_id):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    DELETE FROM bartenders
                    WHERE user_id = %s
                    """,
                    [user_id],
                )

    def get_all_drinks(self):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT id, name, image
                    FROM drinks
                    ORDER BY drinks.id
                """
                )

                results = []
                for row in cur.fetchall():
                    record = {}
                    for i, column in enumerate(cur.description):
                        record[column.name] = row[i]
                    results.append(record)
                return results

    def bartender_record_to_dict(self, row, description):
        bartender = None
        if row is not None:
            bartender = {}
            bartender_fields = [
                "bartender_id",
                "about",
                "drinks",
            ]
            for i, column in enumerate(description):
                if column.name in bartender_fields:
                    bartender[column.name] = row[i]
            bartender["id"] = bartender["bartender_id"]

            user = {}
            user_fields = [
                "user_id",
                "first",
                "last",
                "phone_number",
                "email",
                # "password",
                "city",
                "state",
            ]
            for i, column in enumerate(description):
                if column.name in user_fields:
                    user[column.name] = row[i]
            user["id"] = user["user_id"]

            bartender["user"] = user
        return bartender
