import os
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from routers import bartenders
from routers import users
from routers import auth
from routers import events


app = FastAPI()

origins = [
    "https://mix-n-dash.gitlab.io",
    "http://localhost:3000",
    os.environ.get("CORS_HOST", None),
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(auth.authenticator.router)
app.include_router(users.router)
app.include_router(bartenders.router)
app.include_router(events.router)
