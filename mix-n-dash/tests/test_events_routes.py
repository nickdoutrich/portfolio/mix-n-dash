from fastapi.testclient import TestClient
from main import app
from routers.events import EventQueries
import json

client = TestClient(app)


class EventQueriesMock:
    def get_all_events(self):
        return []

    def get_event(self, event):
        response = {}
        response.update(event)
        return response


def test_events_list():
    # arrange
    app.dependency_overrides[EventQueries] = EventQueriesMock
    # act
    response = client.get("/api/events/")

    # assert
    assert response.status_code == 200
    # 1. get  a  200
    # 2. should *call* queries.get_events()
    assert response.json() == {"events": []}
    app.dependency_overrides = {}
