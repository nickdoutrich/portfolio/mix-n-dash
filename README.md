## Mix n Dash

- Nick Doutrich
- Ray Khan
- Jose Lopez
- Cristoval Ramirez

## Design [Should point to design docs ie wireframe documentation]

- [API design] [ex. docs/apiDesign]
- [Data model]
- [GHI]
- ​

## Intended market

Our platform is able to connect event hosts and bartenders. For those who are hosting an upcoming event for a variety of occasions (birthday, wedding, etc.), they are able to go on the website, create an account, and book a bartender that meets their needs. Additionally, an individual can create an account and choose to become a bartender if they wish to offer bartending services.
​

## Functionality

With no login, a visitor is able to:

- view the entire roster of bartenders
- see bartender detail page
  - see name/about me/location/drinks for each bartender
- choose to create an account

Once account is created, the user can:

- book a bartender for a future event
- choose to become a bartender on the platform
- select one or more drinks for their bartender profile

## Stretch goals

- pricing component for each bartender (hourly rate, or fixed fee)
  - filter by price range
- go from statewide to multi-state to nationwide
- ability for hosts/bartenders to communicate on the website by sending messages
- Users leave reviews for bartenders, and bartenders leave reviews for users
- Bartenders can create custom drinks and upload an image for the drink
- incorporate external API for financial side (Stripe for example)
- bartender being able to serve multiple cities
- build out a footer
  - include things such as About Us section and Contact Us section
- build out the availability (date and time) for each bartender

## Set up

1. Clone: https://gitlab.com/jalopez57/mix-n-dash
2. Create volume with the following command in terminal: docker volume create postgres-data
3. Build the containers and images: docker compose build
-  If you have Apple Mac M1, use this command: DOCKER_DEFAULT_PLATFORM=linux/amd64 docker-compose build
4. Start the containers: docker compose up
5. Open up web browser and go to: http://localhost:3000/
   Success!

​

## Tests

- tests/test_user_routes.py / Nick Doutrich
- tests/test_bartenderlist_routes.py / Ray Khan
- tests/test_events_routes.py / Cristoval Ramirez
